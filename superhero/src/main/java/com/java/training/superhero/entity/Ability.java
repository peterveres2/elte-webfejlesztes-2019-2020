package com.java.training.superhero.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Ability {

	@Id
	@GeneratedValue
	private long id;
	
	private String description;
	
	public Ability (String description) {
		this.description = description;
	}
	
	public Ability() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
