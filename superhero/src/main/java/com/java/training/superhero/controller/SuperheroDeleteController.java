package com.java.training.superhero.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.java.training.superhero.service.SuperheroService;

@Controller
public class SuperheroDeleteController {

	public static final String REQUEST_MAPPING = "superhero-delete";

	@Autowired
	private SuperheroService superheroService;

	@RequestMapping(REQUEST_MAPPING)
	public String deleteSuperHero(@RequestParam("id") Long id) {
		superheroService.deleteSuperheroById(id);
		return "redirect:superheroes";
	}
}