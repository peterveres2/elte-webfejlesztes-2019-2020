package com.java.training.superhero.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.java.training.superhero.entity.Superhero;

public interface SuperheroRepository extends CrudRepository<Superhero, Long> {

	Optional<Superhero> findByName(String name);
}
