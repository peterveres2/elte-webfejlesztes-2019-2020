package com.java.training.superhero.sevlet;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import com.java.training.superhero.entity.Superhero;
import com.java.training.superhero.service.SuperheroService;

@WebServlet(urlPatterns = "/heroDetails/*")
public class HeroDetailsServlet extends HttpServlet {

	
	@Autowired
	private SuperheroService superheroService;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Optional<Superhero> hero 
			= superheroService.findSuperheroByName("Thor");
		if (hero.isPresent()) {
			req.setAttribute("hero", hero.get());
			req.getRequestDispatcher("heroDetails.jsp")
				.forward(req, resp);			
		}
		else {
			resp.getWriter().print("Can't find hero");
		}
	}
}
