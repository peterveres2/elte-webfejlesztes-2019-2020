package com.java.training.superhero.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.java.training.superhero.entity.Superhero;
import com.java.training.superhero.service.SuperheroService;

@Controller
public class SuperheroListController {

	public static final String REQUEST_MAPPING = "superheroes";

	@Autowired
	private SuperheroService superheroService;

	@RequestMapping(value = { REQUEST_MAPPING })
	public String showSuperHeroes(Model model, HttpServletRequest req) {
		initModel(model);
		return "superheroes";
	}

	private void initModel(Model model) {
		Iterable<Superhero> superheroes = superheroService.findAllSuperhero();
		model.addAttribute("superheroes", superheroes);
	}
}
