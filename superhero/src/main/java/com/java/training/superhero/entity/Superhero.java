package com.java.training.superhero.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Superhero {
	
	@Id
	@GeneratedValue
	private long id;
	
	private String name;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "weapon")
	private Weapon weapon;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(
			name = "Superhero_Abilities",
			joinColumns = @JoinColumn(name = "superhero_id"),
			inverseJoinColumns = @JoinColumn(name = "ability_id")
			)
	private List<Ability> listOfAbilities = new ArrayList<>();
	
	public Superhero(String name) {
		this.name = name;
	}
	
	public Superhero() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Weapon getWeapon() {
		return weapon;
	}

	public void setWeapon(Weapon weapon) {
		this.weapon = weapon;
	}

	public List<Ability> getListOfAbilities() {
		return listOfAbilities;
	}

	public void setListOfAbilities(List<Ability> listOfAbilities) {
		this.listOfAbilities = listOfAbilities;
	}
	

}
