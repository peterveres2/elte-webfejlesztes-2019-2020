package com.java.training.superhero.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.java.training.superhero.entity.Superhero;
import com.java.training.superhero.service.SuperheroService;

@Controller
public class SuperheroDetailsController {

	public static final String REQUEST_MAPPING = "superhero-details";

	@Autowired
	private SuperheroService superheroService;

	@RequestMapping(REQUEST_MAPPING)
	public String showDetails(Model model, @RequestParam("id") Long id) {
		initModel(model, id);
		return "superhero-details";
	}

	private void initModel(Model model, Long id) {
		Superhero superhero = superheroService.findSuperheroById(id).get();
		model.addAttribute("superhero", superhero);
	}
}
