package com.java.training.superhero.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.java.training.superhero.entity.Superhero;
import com.java.training.superhero.service.SuperheroService;

@Controller
public class SuperheroUpdateController {

	public static final String REQUEST_MAPPING = "superhero-update";

	@Autowired
	private SuperheroService superheroService;

	@RequestMapping(REQUEST_MAPPING)
	public String updateSuperHero(@RequestParam("id") Long id, @RequestParam("name") String name) {
		update(id, name);
		return "redirect:superheroes";
	}

	private void update(Long id, String name) {
		Superhero superhero = superheroService.findSuperheroById(id).get();
		superhero.setName(name);
		superheroService.saveSuperhero(superhero);
	}
}
