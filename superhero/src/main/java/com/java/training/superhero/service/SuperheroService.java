package com.java.training.superhero.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.training.superhero.entity.Ability;
import com.java.training.superhero.entity.Superhero;
import com.java.training.superhero.entity.Weapon;
import com.java.training.superhero.repository.SuperheroRepository;

@Service
public class SuperheroService {

	@Autowired
	SuperheroRepository superheroRepository;
	
	public void createSuperheroes() {
		
		Superhero ironMan = new Superhero("Iron Man");
		ironMan.setWeapon(new Weapon("Iron Man Suit"));
		ironMan.getListOfAbilities().add(new Ability("genius intellect"));
		ironMan.getListOfAbilities().add(new Ability("engineering"));
		
		superheroRepository.save(ironMan);
		
		Superhero thor = new Superhero("Thor");
		thor.setWeapon(new Weapon("Mjölnir"));
		thor.getListOfAbilities().add(new Ability("lightning strike"));
		thor.getListOfAbilities().add(new Ability("hammer bash"));
		
		superheroRepository.save(thor);
		
		Superhero captainAmerica = new Superhero("Captain America");
		captainAmerica.setWeapon(new Weapon("shield"));
		captainAmerica.getListOfAbilities().add(new Ability("inhuman strength"));
		
		superheroRepository.save(captainAmerica);
		
	}
	
	public Optional<Superhero> findSuperheroByName(String name) {
		return superheroRepository.findByName(name);
	}
	
	public void printSuperheroDetails(String name) {
		
		System.out.println("--------");
		
		Superhero hero = findSuperheroByName(name).get();
			
		System.out.println(hero.getId());
		System.out.println(hero.getName());
		System.out.println(hero.getWeapon());
		
		for (Ability ability : hero.getListOfAbilities()) {
			
			System.out.println(ability.getDescription());
		}
			

		System.out.println("----------");
	}

	public Iterable<Superhero> findAllSuperhero() {
		return superheroRepository.findAll();
	}

	public void deleteSuperheroById(Long id) {
		superheroRepository.deleteById(id);
		
	}

	public Optional<Superhero> findSuperheroById(Long id) {
		return superheroRepository.findById(id);
	}

	public void saveSuperhero(Superhero superhero) {
		superheroRepository.save(superhero);
		
	}
}
