package com.java.training.superhero.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Weapon {

	@Id
	@GeneratedValue
	private long id;
	
	private String weaponName;
	
	@OneToOne(mappedBy = "weapon")
	private Superhero superhero;
	
	public Weapon(String weaponName) {
		this.weaponName = weaponName;
	}
	
	public Weapon() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getWeaponName() {
		return weaponName;
	}

	public void setWeaponName(String weaponName) {
		this.weaponName = weaponName;
	}

	public Superhero getSuperhero() {
		return superhero;
	}

	public void setSuperhero(Superhero superhero) {
		this.superhero = superhero;
	}
	
}
