package com.java.training.superhero;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ConfigurableApplicationContext;

import com.java.training.superhero.service.SuperheroService;

@SpringBootApplication
@ServletComponentScan
public class SuperheroApplication {
	
	@Autowired
	private SuperheroService superheroService;

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(SuperheroApplication.class, args);		
		SuperheroApplication app = context.getBean(SuperheroApplication.class);
		app.play();
	}
	
	public void play() {
		superheroService.createSuperheroes();
		superheroService.printSuperheroDetails("Thor");
	}

}
