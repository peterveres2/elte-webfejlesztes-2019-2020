<!DOCTYPE html>
<html>
<body>

<h1>Hero details page</h1>
<p>These are the details of my hero.</p>

<table border = "1">
	<tr>
		<td>Id</td>
		<td>${hero.id}</td>
	</tr>
	<tr>
		<td>Name</td>
		<td>${hero.name}</td>
	</tr>
	<tr>
		<td>Weapon</td>
		<td>${hero.weapon.weaponName}</td>
	</tr>
</table>
</body>
</html>