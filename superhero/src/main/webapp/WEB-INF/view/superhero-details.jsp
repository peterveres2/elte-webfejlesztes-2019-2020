<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Superheroes</title>
</head>
<body>
	<h1>Superhero details</h1>
	<form method="post" action="<c:url value='superhero-update' />">
		<input type="hidden" value="${superhero.id}" name="id"/>
		<div>
			<label for="name">Name: </label> <input type="text" name="name"
				id="name" value="${superhero.name}">
		</div>
		<div>
			<label for="weapon">Weapon: </label> <span id="weapon">
				${superhero.weapon.weaponName} </span>
		</div>
		<div>
			<label for="abilities">Abilities: </label> <span id="abilities">
				<c:forEach var="ability" items="${superhero.listOfAbilities}">
								${ability.description} 
							</c:forEach>
			</span>
		</div>
		<button type="submit">Save</button>
	</form>
</body>
</html>