<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Superheroes</title>
</head>
<body>
	<h1>Superheroes</h1>

	<table border="1">
		<thead>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Weapon name</th>
				<th>Abilities</th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="superhero" items="${superheroes}">
				<tr>
					<th>${superhero.id}</th>
					<th>${superhero.name}</th>
					<th>${superhero.weapon.weaponName}</th>
					<th><c:forEach var="ability"
							items="${superhero.listOfAbilities}">
							${ability.description} <br />
						</c:forEach></th>
					<th><a
						href="<c:url value='superhero-details?id=${superhero.id}' />">details</a>
					</th>
					<th><a
						href="<c:url value='superhero-delete?id=${superhero.id}' />">delete</a>
					</th>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>