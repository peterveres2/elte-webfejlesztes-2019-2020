package com.epam.edu.servletscopesexample.requestscope.interceptor;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class AddRequestIdInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory.getLogger(AddRequestIdInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
    	String attributeName = "requestId";
		String attributeValue = getUniqueId();
		request.setAttribute(attributeName, attributeValue);
		logger.info("[" + request.getRequestURI() + "] Set " + attributeName + " to '" + attributeValue + "'");
        return true;
    }

	private String getUniqueId() {
		return UUID.randomUUID().toString().substring(0, 7);
	}

}
