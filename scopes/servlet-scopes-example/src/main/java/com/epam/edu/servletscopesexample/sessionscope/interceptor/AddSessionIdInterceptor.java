package com.epam.edu.servletscopesexample.sessionscope.interceptor;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class AddSessionIdInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory.getLogger(AddSessionIdInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
    	String attributeName = "sessionId";
		String attributeValue = getUniqueId();

    	HttpSession session = request.getSession();
		if (session.getAttribute(attributeName) == null) {
			session.setAttribute(attributeName, attributeValue);
		}

		logger.info("[" + request.getRequestURI() + "] Set " + attributeName + " to '" + attributeValue + "'");
        return true;
    }

	private String getUniqueId() {
		return UUID.randomUUID().toString().substring(0, 7);
	}

}
