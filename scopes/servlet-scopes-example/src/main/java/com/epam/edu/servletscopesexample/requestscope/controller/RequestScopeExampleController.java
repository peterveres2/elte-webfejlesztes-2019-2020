package com.epam.edu.servletscopesexample.requestscope.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RequestScopeExampleController {

	private static final Logger logger = LoggerFactory.getLogger(RequestScopeExampleController.class);

	@GetMapping("/request-attribute-example")
	@ResponseBody
	public String getRequestAttribute(@RequestAttribute("requestId") String attributeValue) {
		logger.info("Read requestId: value is '" + attributeValue + "'");
		return attributeValue;
	}

}
