package com.epam.edu.servletscopesexample.applicationscope.controller;

import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ApplicationScopeExampleController {

	private static final Logger logger = LoggerFactory.getLogger(ApplicationScopeExampleController.class);

	@GetMapping("/application-attribute-example")
	@ResponseBody
	public String getRequestAttribute(HttpServletRequest request) {
		String attributeName = "applicationAttribute";
		String attributeValue = "";

		ServletContext servletContext = request.getServletContext();
		if (servletContext.getAttribute(attributeName) == null) {
			String uniqueId = getUniqueId();
			servletContext.setAttribute(attributeName, uniqueId);
			logger.info("Set " + attributeName + " to '" + uniqueId + "'");
		} else {
			attributeValue = (String) servletContext.getAttribute(attributeName);
			logger.info("Read " + attributeName + ": value is '" + attributeValue + "'");
		}

		return attributeValue;
	}

	private String getUniqueId() {
		return UUID.randomUUID().toString().substring(0, 7);
	}

}
