package com.epam.edu.servletscopesexample.requestscope.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.epam.edu.servletscopesexample.requestscope.interceptor.AddRequestIdInterceptor;
import com.epam.edu.servletscopesexample.requestscope.interceptor.ReadRequestIdInterceptor;

@Profile("request-scope-example")
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new AddRequestIdInterceptor());
		registry.addInterceptor(new ReadRequestIdInterceptor());
	}

}
