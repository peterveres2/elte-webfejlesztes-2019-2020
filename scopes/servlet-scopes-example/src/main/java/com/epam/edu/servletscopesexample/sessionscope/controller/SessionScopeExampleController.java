package com.epam.edu.servletscopesexample.sessionscope.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;

@Controller
public class SessionScopeExampleController {

	private static final Logger logger = LoggerFactory.getLogger(SessionScopeExampleController.class);

	@GetMapping("/session-attribute-example")
	@ResponseBody
	public String getRequestAttribute(@SessionAttribute("sessionId") String attributeValue) {
		logger.info("Read sessionId: value is '" + attributeValue + "'");
		return attributeValue;
	}

}
