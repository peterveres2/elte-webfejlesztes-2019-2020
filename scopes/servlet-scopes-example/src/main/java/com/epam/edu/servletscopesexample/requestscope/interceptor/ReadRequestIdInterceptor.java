package com.epam.edu.servletscopesexample.requestscope.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class ReadRequestIdInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory.getLogger(ReadRequestIdInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
    	String attributeName = "requestId";
		String attributeValue = (String) request.getAttribute(attributeName);
		logger.info("[" + request.getRequestURI() + "] Read " + attributeName + ": value is '" + attributeValue + "'");
        return true;
    }

}
