package com.epam.edu.servletscopesexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServletScopesExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServletScopesExampleApplication.class, args);
	}

}
