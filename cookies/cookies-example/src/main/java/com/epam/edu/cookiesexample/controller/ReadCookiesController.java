package com.epam.edu.cookiesexample.controller;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class ReadCookiesController {

	@ModelAttribute("cookies")
	public List<String> readAllCookies(HttpServletRequest request) {
		List<String> result = Collections.emptyList();
	    Cookie[] cookies = request.getCookies();
	    if (cookies != null) {
	    	result = Arrays.stream(cookies)
	                .map(c -> c.getName() + "=" + c.getValue())
	                .collect(Collectors.toList());
	    }
	    return result;
	}


	@GetMapping("/readcookies")
	public String createReadCookiesPage() {
		return "read-cookies";
	}

	@GetMapping("/readcookies/js")
	public String createReadCookiesJsPage() {
		return "read-cookies-js";
	}

}
