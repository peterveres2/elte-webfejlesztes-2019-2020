package com.epam.edu.cookiesexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CookiesExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(CookiesExampleApplication.class, args);
	}

}
