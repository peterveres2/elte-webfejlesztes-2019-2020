INSERT INTO users (username, name, manager, password, enabled) VALUES
	('bob_smith', 		'Bob Smith', 		NULL, 			'{noop}password', 1),
	('juan_perez', 		'Juan Perez', 		NULL, 			'{noop}password', 1),
	('john_doe', 		'John Doe', 		'bob_smith', 	'{noop}password', 0),
	('average_joe', 	'Average Joe', 		'bob_smith', 	'{noop}password', 0),
	('larry_lead_foot', 'Larry Lead-Foot', 	'bob_smith', 	'{noop}password', 0),
	('sally_housecoat', 'Sally Housecoat', 	'juan_perez', 	'{noop}password', 0),
	('jack_daws', 		'Jack Daws', 		'juan_perez', 	'{noop}password', 0);

INSERT INTO authorities (username, authority) values
	('bob_smith', 'ROLE_MANAGER'),
	('juan_perez', 'ROLE_MANAGER'),
	('john_doe', 'ROLE_EMPLOYEE'),
	('average_joe', 'ROLE_EMPLOYEE'),
	('larry_lead_foot', 'ROLE_EMPLOYEE'),
	('sally_housecoat', 'ROLE_EMPLOYEE'),
	('jack_daws', 'ROLE_EMPLOYEE');
