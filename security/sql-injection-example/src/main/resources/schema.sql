CREATE TABLE users (
  id UUID DEFAULT RANDOM_UUID(),
  username VARCHAR(50) NOT NULL,
  name VARCHAR(50) NOT NULL,
  password VARCHAR(100) NOT NULL,
  manager VARCHAR(50),
  enabled TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (id),
  FOREIGN KEY (manager) REFERENCES users(username) ON DELETE CASCADE
);

CREATE TABLE authorities (
  username VARCHAR(50) NOT NULL,
  authority VARCHAR(50) NOT NULL,
  FOREIGN KEY (username) REFERENCES users(username) ON DELETE CASCADE
);

CREATE UNIQUE INDEX ix_auth_username ON authorities (username, authority);
