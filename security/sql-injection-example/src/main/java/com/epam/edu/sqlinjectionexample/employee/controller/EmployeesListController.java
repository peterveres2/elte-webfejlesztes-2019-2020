package com.epam.edu.sqlinjectionexample.employee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.epam.edu.sqlinjectionexample.employee.dao.EmployeeDao;
import com.epam.edu.sqlinjectionexample.employee.domain.Employee;

@Controller
public class EmployeesListController {

	private EmployeeDao employeeDao;

	@Autowired
	public void setEmployeeDao(EmployeeDao employeeDao) {
		this.employeeDao = employeeDao;
	}

	@ModelAttribute("employees")
	public List<Employee> getEmployees() {
		return employeeDao.findAllEmployees();
	}

	@GetMapping("/employees")
	public String getEmployeesListPage() {
		return "employees";
	}

}
