package com.epam.edu.sqlinjectionexample.subordinate.domain;

public class Subordinate {

	private String name;

	private String id;

	public Subordinate(String name, String id) {;
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public String getId() {
		return id;
	}

}
