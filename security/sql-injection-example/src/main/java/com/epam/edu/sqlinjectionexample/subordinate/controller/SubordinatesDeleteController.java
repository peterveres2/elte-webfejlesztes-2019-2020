package com.epam.edu.sqlinjectionexample.subordinate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.edu.sqlinjectionexample.subordinate.dao.SubordinateDao;

@Controller
public class SubordinatesDeleteController {

	private SubordinateDao subordinateDao;

	@Autowired
	public void setSubordinateDao(SubordinateDao subordinateDao) {
		this.subordinateDao = subordinateDao;
	}

	@PostMapping("/subordinates/delete")
	public String deleteSubordinateController(@RequestParam("id") String id) {
		subordinateDao.deleteSubordinateById(id);
		return "redirect:/subordinates";
	}

}
