package com.epam.edu.sqlinjectionexample.subordinate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.epam.edu.sqlinjectionexample.subordinate.dao.SubordinateDao;
import com.epam.edu.sqlinjectionexample.subordinate.domain.Subordinate;

@Controller
public class SubordinatesListController {

	private SubordinateDao subordinateDao;

	@Autowired
	public void setSubordinateDao(SubordinateDao subordinateDao) {
		this.subordinateDao = subordinateDao;
	}

	@ModelAttribute("subordinates")
	public List<Subordinate> getSubordinates(Authentication authentication) {
		return subordinateDao.findAllSubordinatesOf(authentication.getName());
	}

	@GetMapping("/subordinates")
	public String getSubordinatesListPage() {
		return "subordinates";
	}

}
