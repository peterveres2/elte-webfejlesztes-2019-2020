package com.epam.edu.sqlinjectionexample.employee.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.epam.edu.sqlinjectionexample.employee.domain.Employee;

@Component
public class EmployeeDao {

	private static final String EMPLOYEES_QUERY = "SELECT employees.name, managers.name AS manager FROM users employees "
												+ "LEFT JOIN users managers ON employees.manager = managers.username";
	private JdbcTemplate jdbcTemplate;

	@Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public List<Employee> findAllEmployees() {
    	return jdbcTemplate.query(EMPLOYEES_QUERY, (rs, rowNum) -> {
    		return new Employee(rs.getString("name"), rs.getString("manager"));
    	});
    }

}
