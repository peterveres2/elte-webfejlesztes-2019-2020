package com.epam.edu.sqlinjectionexample.subordinate.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.epam.edu.sqlinjectionexample.subordinate.domain.Subordinate;

@Component
public class SubordinateDao {

	private static final String SUBORDINATES_QUERY = "SELECT id, name FROM users WHERE manager = ?";
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public List<Subordinate> findAllSubordinatesOf(String manager) {
		return jdbcTemplate.query(SUBORDINATES_QUERY, new Object[] { manager }, (resultSet, rowNum) -> {
			return new Subordinate(resultSet.getString("name"), resultSet.getString("id"));
		});
	}

	// !! DO NOT DO THIS !!
	public void deleteSubordinateById(String id) {
		jdbcTemplate.execute("DELETE FROM users WHERE id = '" + id + "'");
	}

}
