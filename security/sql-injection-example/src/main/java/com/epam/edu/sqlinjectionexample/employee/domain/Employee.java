package com.epam.edu.sqlinjectionexample.employee.domain;

public class Employee {

	private String name;

	private String manager;

	public Employee(String name, String manager) {
		this.name = name;
		this.manager = manager;
	}

	public String getName() {
		return name;
	}

	public String getManager() {
		return manager;
	}

}
