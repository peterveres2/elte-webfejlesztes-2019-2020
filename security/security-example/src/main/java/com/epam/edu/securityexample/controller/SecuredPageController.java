package com.epam.edu.securityexample.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SecuredPageController {

	@GetMapping("/secured")
	public String getSecuredPage() {
		return "secured";
	}

}
