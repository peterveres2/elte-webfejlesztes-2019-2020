package com.epam.edu.storedxssexample.comments.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.edu.storedxssexample.comments.dao.CommentDao;
import com.epam.edu.storedxssexample.comments.domain.Comment;

@Controller
public class CommentPostController {

	private CommentDao commentDao;

	@Autowired
	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}

	@PostMapping("/comment")
	public String handlePostComment(@RequestParam("text") String text, Authentication authentication) {
		commentDao.addComment(new Comment(authentication.getName(), text));
		return "redirect:/";
	}

}
