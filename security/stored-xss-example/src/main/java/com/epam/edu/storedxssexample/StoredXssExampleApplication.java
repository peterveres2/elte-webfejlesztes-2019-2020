package com.epam.edu.storedxssexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StoredXssExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(StoredXssExampleApplication.class, args);
	}

}
