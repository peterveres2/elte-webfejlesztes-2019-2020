package com.epam.edu.storedxssexample.comments.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.epam.edu.storedxssexample.comments.dao.CommentDao;
import com.epam.edu.storedxssexample.comments.domain.Comment;

@Controller
public class CommentsPageController {

	private CommentDao commentDao;

	@Autowired
	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}

	@ModelAttribute("comments")
	public List<Comment> getComments() {
		return commentDao.findAllComments();
	}

	@GetMapping("/")
	public String getCommentsPage() {
		return "comments";
	}

}
