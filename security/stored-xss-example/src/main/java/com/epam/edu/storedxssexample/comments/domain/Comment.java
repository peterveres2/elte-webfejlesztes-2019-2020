package com.epam.edu.storedxssexample.comments.domain;

public class Comment {

	private String text;

	private String author;

	public Comment(String author, String text) {
		this.author = author;
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public String getAuthor() {
		return author;
	}

}
