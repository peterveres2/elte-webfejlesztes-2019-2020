package com.epam.edu.storedxssexample.comments.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.epam.edu.storedxssexample.comments.domain.Comment;

@Component
public class CommentDao {

	private static final String COMMENTS_QUERY = "SELECT username, comment FROM comments";
	private static final String ADD_COMMENT_QUERY = "INSERT INTO comments (username, comment) VALUES (?, ?)";
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public List<Comment> findAllComments() {
		return jdbcTemplate.query(COMMENTS_QUERY, (rs, rowNum) -> {
			return new Comment(rs.getString("username"), rs.getString("comment"));
		});
	}

	public void addComment(Comment comment) {
		jdbcTemplate.update(ADD_COMMENT_QUERY, comment.getAuthor(), comment.getText());
	}

}
